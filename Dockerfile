FROM alpine:3.6
LABEL maintainer daniel@harrisbaird.co.uk

ENV RUNTIME_PACKAGES ca-certificates python3 libxslt libxml2 libssl1.0
ENV BUILD_PACKAGES build-base python3-dev libxslt-dev libxml2-dev libffi-dev openssl-dev git

RUN apk add --no-cache $RUNTIME_PACKAGES && \
    update-ca-certificates


RUN apk --no-cache add --virtual build-dependencies $BUILD_PACKAGES

RUN pip3 install pipenv --upgrade

COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock

RUN pipenv install --deploy --system


RUN mkdir /app
WORKDIR /app

COPY . /app


EXPOSE 6800

CMD ["scrapyd"]